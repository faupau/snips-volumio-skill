#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from hermes_python.ontology import *
import requests
import io

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

class SnipsConfigParser(configparser.SafeConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.readfp(f)
            return conf_parser.to_dict()
    except (IOError, configparser.Error) as e:
        return dict()

def subscribe_intent_callback(hermes, intentMessage):
    conf = read_configuration_file(CONFIG_INI)
    action_wrapper(hermes, intentMessage, conf)


def action_wrapper(hermes, intentMessage, conf):
    volumio_host = conf['global'].get("volumio_host")                #getting volumio host ip from config.ini
    requests.get('http://{}/api/v1/commands/?cmd=play'.format(volumio_server))   #sending play to volumio
    result_sentence = "Ok"
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, result_sentence)

if __name__ == "__main__":
    conf = read_configuration_file(CONFIG_INI)
    mqtt_server = conf['global'].get("mqtt_server")
    mqtt_opts = MqttOptions()
    with Hermes("{}:1883".format(mqtt_server)) as h:
        h.subscribe_intent("CryptoWarrior:play", subscribe_intent_callback) \
.start()
